﻿#include "pch.h"
#include <iostream>
#include <string>
#include <queue>

using namespace std;

enum MachineState { STATEON, STATEOFF, STATEPLAYBACKSTOPPED, STATEPLAYING, DUMBSTATE };
static MachineState StateOnFamily[3] = { STATEPLAYBACKSTOPPED, STATEPLAYING, STATEON };
struct msg
{
	MachineState state; //struct - for future
};

void QueueSend(queue <msg> &QueueHandle)
{
	cout << "Choose the state:" << endl;
	cout << "0 - STATEON" << endl;
	cout << "1 - STATEOFF" << endl;
	cout << "2 - STATEPLAYBACKSTOPPED" << endl;
	cout << "3 - STATEPLAYING" << endl;
	int chosen;
	cin >> chosen;
	msg temporaryMsg;
	switch (chosen)
	{
	case 0:
		temporaryMsg.state = STATEON;
		QueueHandle.push(temporaryMsg);
		break;
	case 1:
		temporaryMsg.state = STATEOFF;
		QueueHandle.push(temporaryMsg);
		break;
	case 2:
		temporaryMsg.state = STATEPLAYBACKSTOPPED;
		QueueHandle.push(temporaryMsg);
		break;
	case 3:
		temporaryMsg.state = STATEPLAYING;
		QueueHandle.push(temporaryMsg);
		break;
	default:
		cout << "Incorrect request" << endl;
	}
}
void QueueReceive(queue <msg> &QueueHandle, MachineState *CurrentState)
{
	if (!QueueHandle.empty())
	{
		msg ReceivedMsg = QueueHandle.front();
		QueueHandle.pop();
		*CurrentState = ReceivedMsg.state;
	}
	else
	{
		cout << "Incorrect state request! Queue is empty!" << endl;
	}
}
void Action(MachineState current)
{
	switch (current)
	{
	case STATEON:
		cout << "This is a parent!" << endl; //should be in child!
		break;
	case STATEOFF:
		cout << "STATUS: POWEROFF" << endl;
		break;
	case STATEPLAYBACKSTOPPED:
		cout << "STATUS: STATEPLAYBACKSTOPPED" << endl;
		break;
	case STATEPLAYING:
		cout << "STATUS: STATEPLAYING" << endl;
		break;
	default:
		cout << "STH WENT WRONG!" << endl;
	}
	cout << "FINISHED" << endl;
}
//initial state

int main()
{
	MachineState currentState = STATEOFF;
	MachineState nextState;
	queue <msg> QueueHandle;
	while (1)
	{
		QueueSend(QueueHandle);
		QueueReceive(QueueHandle, &nextState);
		switch (nextState)
			{
			case STATEOFF:
				currentState = STATEOFF;
				break;
			case STATEON:
				if (find(StateOnFamily, StateOnFamily + 2, currentState) == StateOnFamily + 2)
				{
					currentState = STATEPLAYBACKSTOPPED;
					cout << "STATUS: POWER ON -> STATEPLAYBACKSTOPPED" << endl;
				}
				else
				{
					cout << "Incorrect Transition" << endl;
				}
				break;
			case STATEPLAYBACKSTOPPED:
				if (find(StateOnFamily, StateOnFamily + 2, nextState) != (StateOnFamily + 2) &&
					find(StateOnFamily, StateOnFamily + 2, currentState) != (StateOnFamily + 2))
				{
					currentState = STATEPLAYBACKSTOPPED;
				}
				else
				{
					cout << "Incorrect transition!" << endl;
				}
				
				break;
			case STATEPLAYING:
				if (find(StateOnFamily, StateOnFamily + 2, nextState) != (StateOnFamily + 2) &&
					find(StateOnFamily, StateOnFamily + 2, currentState) != (StateOnFamily + 2))
				{
					currentState = STATEPLAYING;
				}
				else
				{
					cout << "Incorrect transition!" << endl;
				}
				break;
				//add new states here
			default:
				cout << "ERROR! WRONG STATE SELECTION!" << endl;
				break;
			
		}
		Action(currentState);
	}
	return 0;
}
