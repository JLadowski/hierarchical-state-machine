README
I.	Informacje ogólne:

•	Visual studio 2017 

•	Aplikacja konsolowa

•	Exe znajduje się w folderze ‘debug’ 

II.	Działanie aplikacji HSM:

Pętla główna programu jest w pliku HSM.cpp. W górnej części głównego pliku znajduje się Tablica przejść (req. 4). Jest to wektor par, pierwszy argument pary to stan, w którym znajduje się maszyna, natomiast drugi argument to stan, w który może przejść. Założono, że dozwolone są przejścia:

```
static vector< pair <StateName, StateName> > TransitionTable =
{
	{STATEOFF,STATEON},
	{STATEON,STATEPLAYBACKSTOPPED},
	{STATEON,STATEPLAYING},
	{STATEON,STATEOFF},
	{STATEPLAYBACKSTOPPED,STATEOFF},
	{STATEPLAYBACKSTOPPED,STATEPLAYING},
	{STATEPLAYING,STATEOFF},
	{STATEPLAYING,STATEPLAYBACKSTOPPED}
};
```

Przejścia niezdefiniowane w tabeli będą odrzucane. Tabela jest łatwo modyfikowalna. 
Stan początkowy maszyny to ‘STATEOFF’.

Pierwszą częścią głównej pętli jest do…while. W pętli do..while użytkownik jest proszony 
o wybór następnego stanu. Warunkiem wykonywania pętli jest wartość zwracana przez metodę obiektu ‘Watcher’. Decyduje ona, czy przejście wybrane przez użytkownika jest prawidłowe. Jeśli nie: ponawiane jest pytanie o następny stan. 

W momencie wyboru poprawnego kolejnego stanu, jest on wysyłany do kolejki (req. 1) przez metodę: 

`	queue.QueueSend(QueueHandle, (StateName)chosenState);`
	

Następnie jest odczytywany przez program poprzez metodę: 

`	queue.QueueReceive(QueueHandle, &nextState);`

Następnie odczytywana wartość używana jest poprzez switcha, który obejmuje 4 stany. Przykładowy przypadek: 
	```
case STATEOFF:
	entity.GetHSM()->ChangeState(StateOffState::Instance());
	entity.Update();
	currentState = STATEOFF;
	break;
    ```



Dodanie kolejnego stanu (req. 2):

a.	Dodanie nazwy do: 

```
enum StateName { STATEON, STATEOFF, STATEPLAYBACKSTOPPED, STATEPLAYING };

```

w pliku ‘Watcher.h’
    
b.	Dodanie przejść do TransitionTable w pliku HSM.cpp

c.	Dodanie kolejnej klasy w pliku EntityState.h

d.	Oprogramowanie metod ‘Enter’, ‘Execute’ oraz ‘Exit’ w pliku EntityState.cpp

e.	(do testów!) Dodanie opisu w petli do…while



III.	Działanie aplikacji C-Style:

The app is created to easilly check the transitions between states. There are 4 states:

•	STATEOFF

•	STATEON

•	STATEPLAYBACKSTOPPED

•	STATEPLAYING

First two states are parents. The ‘STATEOFF’ does not contain any children. ‘STATEPLAYBACKSTOPPED’ and ‘STATEPLAYING’ are children of ‘STATEON’ state. When the machine is in state ‘POWERON’, it must be also in one of a children states.
The transition between states are communicated by a queue mechanism. The message is a struct, which contains only one element – a requested state. (the struct is used only because of future using of bigger messages). 
	 Machine receives a message every one cycle:
	 
    1.	Ask user for a next state.
    
    2.	The machine receives the message.
    
    3.	The machine analizes a transistion correctness.
    
    4.	New state set up or a communicate about incorrect request.
    

The delivery of a message is created by function: 


TASKS:
1.	The queue is implemented. There are 2 functions: 


```
void QueueSend(queue <msg> &QueueHandle);

void QueueReceive(queue <msg> &QueueHandle, MachineState *CurrentState);

```


In order to easy check corectness user is asked about the next  state of the machine, so there is no input message pointer in QueueSend function.


2.	How to add a new state: 

•	Add a name in ‘enum MachineState’

•	If the state is goint to have some childred, create table of a family, like: 

`static MachineState StateOnFamily[3] = {STATEPLAYBACKSTOPPED, STATEPLAYING, STATEON };`

•	where the first element would be a parent state. (add children states to ‘enum MachineState’

•	Add states to switch case in main (look for //add new states here)

Remember: Parent state should be in initial child state, like: 
		
```
case STATEON:
    currentState = STATEPLAYBACKSTOPPED;
```


•	Add a proper action in void Action(MachineState current) function.


3.	You can add information in void Action(MachineState current). Also, it is worth to create a function, which contains a whole action of one state.
4. Transition table:

C-Style implementation does not contain an implementation of a transition table. 



