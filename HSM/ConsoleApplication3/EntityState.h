#pragma once
#include "State.h"

class Entity;

class StateOffState:public State<Entity>
{
	StateOffState()
	{
		
	}
	StateOffState&operator=(const StateOffState&);
	StateOffState(const StateOffState&);

public:
	static StateOffState*Instance();
	void Enter(Entity*owner);
	void Execute(Entity*owner);
	void Exit(Entity*owner);
};
class StateOnState :public State<Entity>
{
	StateOnState(){}
	StateOnState&operator=(const StateOnState&);
	StateOnState(const StateOnState&);
public:
	static StateOnState*Instance();
	void Enter(Entity*owner);
	void Execute(Entity*owner);
	void Exit(Entity*owner);
};
class StatePlaybackStoppedState :public State<Entity>
{
	StatePlaybackStoppedState() {}
	StatePlaybackStoppedState&operator=(const StatePlaybackStoppedState&);
	StatePlaybackStoppedState(const StatePlaybackStoppedState&);
public:
	static StatePlaybackStoppedState*Instance();
	void Enter(Entity*owner);
	void Execute(Entity*owner);
	void Exit(Entity*owner);
};
class StatePlayingState :public State<Entity>
{
	StatePlayingState() {}
	StatePlayingState&operator=(const StatePlayingState&);
	StatePlayingState(const StatePlayingState&);
public:
	static StatePlayingState*Instance();
	void Enter(Entity*owner);
	void Execute(Entity*owner);
	void Exit(Entity*owner);
};