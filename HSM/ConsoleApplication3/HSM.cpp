﻿#include "pch.h"
#include <iostream>
#include "Entity.h"
#include "Watcher.h"
#include "queueadmin.h"

using namespace std;


static vector< pair <StateName, StateName> > TransitionTable =
{
	{STATEOFF,STATEON},
	{STATEON,STATEPLAYBACKSTOPPED},
	{STATEON,STATEPLAYING},
	{STATEON,STATEOFF},
	{STATEPLAYBACKSTOPPED,STATEOFF},
	{STATEPLAYBACKSTOPPED,STATEPLAYING},
	{STATEPLAYING,STATEOFF},
	{STATEPLAYING,STATEPLAYBACKSTOPPED}
};
queue <msg> QueueHandle;

void main()
{
	cout << "Hello HSM!\n" << endl;

	StateName currentState=STATEOFF, requestedState;
	StateName nextState; //verified requestedState
	int chosenState; //input (keyboard)
	Entity entity;
	Watcher watcher;
	QueueAdmin queue;
	
	//main loop:
	cout << "Initial State: ";
	entity.Update(); //initial state in Entity.h
	
	while ("Reprezentacja Polski nie zostanie mistrzem świata")
	{
		do
		{
			cout << "Choose state: " << endl;
			cout << "0 - STATEON" << endl;
			cout << "1 - STATEOFF" << endl;
			cout << "2 - STATEPLAYBACKSTOPPED" << endl;
			cout << "3 - STATEPLAYING" << endl;
			cin >> chosenState;
			requestedState = (StateName)chosenState;
		} while (!watcher.checkTransition(currentState, requestedState, TransitionTable));

		queue.QueueSend(QueueHandle, (StateName)chosenState);
		queue.QueueReceive(QueueHandle, &nextState);

		switch (nextState)
		{
			case STATEON:
				entity.GetHSM()->ChangeState(StateOnState::Instance());
				entity.Update();
				entity.GetHSM()->ChangeState(StatePlaybackStoppedState::Instance());
				entity.Update();
				currentState = STATEPLAYBACKSTOPPED;
				break;
			case STATEOFF:
				entity.GetHSM()->ChangeState(StateOffState::Instance());
				entity.Update();
				currentState = STATEOFF;
				break;
			case STATEPLAYBACKSTOPPED:
				entity.GetHSM()->ChangeState(StatePlaybackStoppedState::Instance());
				entity.Update();
				currentState = STATEPLAYBACKSTOPPED;
				break;
			case STATEPLAYING:
				entity.GetHSM()->ChangeState(StatePlayingState::Instance());
				entity.Update();
				currentState = STATEPLAYING;
				break;
			default:
				cout << "STH WENT WRONG" <<endl;
				break;
		}

	}


}

