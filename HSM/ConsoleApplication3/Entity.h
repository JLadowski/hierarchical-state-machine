#include "StateMachine.h"
#include "EntityState.h"

class Entity
{
	StateMachine<Entity>*m_pStateMachine;
public: 
	Entity() 
	{
		m_pStateMachine = new StateMachine<Entity>(this);
		m_pStateMachine->SetCurrentState(StateOffState::Instance());//Set initial state
	}
	~Entity()
	{
		delete m_pStateMachine;
	}

	void Update()
	{
		m_pStateMachine->Update();
	}
	StateMachine<Entity>*GetHSM()
	{
		return this->m_pStateMachine;
	}
};  