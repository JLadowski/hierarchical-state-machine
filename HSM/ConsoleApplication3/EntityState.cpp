#include "pch.h"
#include "EntityState.h"
#include "Entity.h"

#include <iostream>

using namespace std; 

StateOffState*StateOffState::Instance()
{
	static StateOffState instance;
	return &instance;
}
void StateOffState::Enter(Entity*owner)
{
	cout << "Enter StateOffState" << endl;
}
void StateOffState::Execute(Entity*owner)
{
	cout << "Execute StateOffState" << endl;
	owner->GetHSM()->ChangeState(StateOffState::Instance());
}
void StateOffState::Exit(Entity*owner)
{
	cout << "Exit StateOffState" << endl;
}
StateOnState*StateOnState::Instance()
{
	static StateOnState instance;
	return &instance;
}
void StateOnState::Enter(Entity*owner)
{
	cout << "Enter StateOnState" << endl;
}
void StateOnState::Execute(Entity*owner)
{
	cout << "Execute StateOnState" << endl;
}
void StateOnState::Exit(Entity*owner)
{
	cout << "Exit StateOnState" << endl;
}

StatePlaybackStoppedState*StatePlaybackStoppedState::Instance()
{
	static StatePlaybackStoppedState instance;
	return &instance;
}
void StatePlaybackStoppedState::Enter(Entity*owner)
{
	cout << "Enter StatePlaybackStoppedState" << endl;
}
void StatePlaybackStoppedState::Execute(Entity*owner)
{
	cout << "Execute StatePlaybackStoppedState" << endl;
}
void StatePlaybackStoppedState::Exit(Entity*owner)
{
	cout << "Exit StatePlaybackStoppedState" << endl;
}
StatePlayingState*StatePlayingState::Instance()
{
	static StatePlayingState instance;
	return &instance;
}
void StatePlayingState::Enter(Entity*owner)
{
	cout << "Enter StatePlayingState" << endl;
}
void StatePlayingState::Execute(Entity*owner)
{
	cout << "Execute StatePlayingState" << endl;
}
void StatePlayingState::Exit(Entity*owner)
{
	cout << "Exit StatePlayingState" << endl;
}