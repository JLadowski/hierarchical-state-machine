#pragma once
#include <queue>
#include <iostream>

using namespace std;



//enum MachineState { STATEON, STATEOFF, STATEPLAYBACKSTOPPED, STATEPLAYING };
struct msg
{
	StateName state; //struct - for future
};

class QueueAdmin
{

	public:
	void QueueSend(queue <msg> &QueueHandle, StateName chosen)
	{
		msg temporaryMsg;
		switch (chosen)
		{
		case 0:
			temporaryMsg.state = STATEON;
			QueueHandle.push(temporaryMsg);
			break;
		case 1:
			temporaryMsg.state = STATEOFF;
			QueueHandle.push(temporaryMsg);
			break;
		case 2:
			temporaryMsg.state = STATEPLAYBACKSTOPPED;
			QueueHandle.push(temporaryMsg);
			break;
		case 3:
			temporaryMsg.state = STATEPLAYING;
			QueueHandle.push(temporaryMsg);
			break;
		default:
			cout << "Incorrect request" << endl;
		}
	}
	void QueueReceive(queue <msg> &QueueHandle, StateName *CurrentState)
	{
		if (!QueueHandle.empty())
		{
			msg ReceivedMsg = QueueHandle.front();
			QueueHandle.pop();
			*CurrentState = ReceivedMsg.state;
		}
		else
		{
			cout << "Incorrect state request! Queue is empty!" << endl;
		}
	}

};