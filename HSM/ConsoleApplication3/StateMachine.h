#pragma once
#include "State.h"
#include <iostream>

template <class T>
class StateMachine
{
	State<T>* m_pCurrentState;
	T*m_pOwner;
	public:
		StateMachine(T*owner) :m_pOwner(owner), m_pCurrentState(NULL)
		{

		}
		void Update()
		{
			m_pCurrentState->Execute(m_pOwner);
		}
		void ChangeState(State<T>*pState)
		{
			m_pCurrentState->Exit(m_pOwner);
			m_pCurrentState = pState;
			m_pCurrentState->Enter(m_pOwner);
		}
		//Getters and Setters
		State<T>*GetCurrentState() { return this->m_pCurrentState; }
		void SetCurrentState(State<T>*pState) { m_pCurrentState = pState; }

};