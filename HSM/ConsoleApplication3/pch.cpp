﻿// pch.cpp: plik źródłowy odpowiadający wstępnie skompilowanemu nagłówkowi, niezbędny do powodzenia kompilacji

#include "pch.h"

#include "Entity.h"
#include "EntityState.h"
#include "State.h"
#include "StateMachine.h"
#include "Watcher.h"
#include "queueadmin.h"

// Ogólnie rzecz biorąc, zignoruj ten plik, ale miej go pod ręką, jeśli używasz wstępnie skompilowanych nagłówków.
