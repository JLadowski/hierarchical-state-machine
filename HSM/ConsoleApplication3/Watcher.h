#pragma once
#include "State.h"
#include <list>
#include <utility>
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

enum StateName { STATEON, STATEOFF, STATEPLAYBACKSTOPPED, STATEPLAYING };

class Watcher
{
	public:
		int checkTransition(StateName pCurrentState, StateName pNextState, vector<pair <StateName, StateName>> &vect)
		{
			int permission=0;
			for (int i = 0; i < vect.size(); i++)
			{
				if (vect[i] == make_pair(pCurrentState, pNextState))
				{
					permission = 1;
					break;
				}
			}
			if(!permission) cout << "Invalid Transition!" << endl;
			return permission;
		}
	~Watcher()
	{
		//delete Watcher;
	}
};




//static StateName TransitionTable[][2] = 
//{
//	{STATEOFF,STATEON},
//	{STATEOFF,STATEPLAYBACKSTOPPED},
//	{STATEOFF,STATEPLAYING},
//	{STATEON,STATEPLAYBACKSTOPPED},
//	{STATEON,STATEPLAYING},
//	{STATEON,STATEOFF},
//	{STATEPLAYBACKSTOPPED,STATEOFF},
//	{STATEPLAYBACKSTOPPED,STATEPLAYING},
//	{STATEPLAYING,STATEOFF},
//	{STATEPLAYING,STATEPLAYING},
//}
